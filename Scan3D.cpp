﻿#include "Scan3D.h"

void display(cv::Mat processedFrame, std::string windowName) {
    // Start viewing
    cv::imshow(windowName, processedFrame);
    int key = cv::waitKey(1);
    if (key == ' ') key = 0;  // handled
    if (key > 0) exit(EXIT_SUCCESS);
}

void sequential(bool webcam=false) {
    if (webcam) {
        VideoCapture _cap;

        // Initialize the default camera with OpenCV.
        _cap.open(1, cv::CAP_ANY);

        while (true) {
            cv::Mat frame;
            if (_cap.isOpened()) {
                auto frameWidth = int(_cap.get(CAP_PROP_FRAME_WIDTH));
                auto frameHeight = int(_cap.get(CAP_PROP_FRAME_HEIGHT));
                cout << "Video width=" << frameWidth
                    << ", height=" << frameHeight << ",  Backend: " << _cap.getBackendName() << endl;
            }
            else {
                std::cerr << "Error: Could not open the camera." << std::endl;
                exit(EXIT_FAILURE);
            }

            if (!_cap.read(frame)) {
                cout << "Cant read." << endl;
                Sleep(5000);
                exit(EXIT_FAILURE);
            }
            cv::imshow("Live", frame);
            if (cv::waitKey(1) == 'q') exit(EXIT_SUCCESS);
        }
        _cap.release();
    }
    else {
        // Search for Nerian stereo devices
        DeviceEnumeration deviceEnum;
        auto _devices = deviceEnum.discoverDevices();

        AsyncTransfer obj(_devices[0]);
        ImageSet _frame;

        while (true) {
            // loop until a frame is captured.
            while (!obj.collectReceivedImageSet(_frame, 0.1 /*timeout*/));

            cv::Mat _conFrame;
            _frame.toOpenCVImage(0, _conFrame);
            ReconstructionMod reconstructor;
            DetectionMod detector;
            detector.detectAndRecognizeFaces(_conFrame);
            cv::imshow("Detect", detector._result);

            // Break the loop if 'a key'q' is pressed
            if (waitKey(1) == 'q') exit(EXIT_SUCCESS);
        }
    }
}

std::queue<cv::Mat> frames; 
std::mutex mtx11;

cv::VideoCapture initCam() {
    cv::VideoCapture _cap;

    // Initialize the default camera with OpenCV.
    _cap.open(1, cv::CAP_ANY);

    if (!_cap.isOpened()) {
        std::cerr << "Error: Could not open the camera." << std::endl;
        exit(EXIT_FAILURE);
    }
    return _cap;
}

void grab(cv::VideoCapture cam) {
    //std::lock_guard<std::mutex> lock(mtx11);
    cv::Mat frame;

    // Capture a frame via the default camera
    cam >> frame;
    frames.push(frame);
}
void threadShow() {
    if (frames.empty()) {
        cout<<"waiting..."<< std::endl;
        Sleep(3000);
        exit(EXIT_FAILURE);
    }
}

int main() {
    // Create instances of the modules
    Publisher frameGrabber;
    ReconstructionMod reconstructor;
    DetectionMod detector;

    AsyncTransfer transferObj(frameGrabber._devices[0]);

    // Subscribe modules to events
    frameGrabber.addSubscriber(&reconstructor);
    frameGrabber.addSubscriber(&detector);

    auto cam = initCam();
    
    while (true) {
        frameGrabber.capture(transferObj);
        //reconstructor.captureDepthMap();
        //detector.process();

        std::thread detection(&DetectionMod::process, &detector);
        std::thread depth(&ReconstructionMod::captureDepthMap, &reconstructor);

        detection.detach();
        depth.detach();
        Sleep(160);

        cv::imshow("Face Recognition", detector._result);
        cv::imshow("Depth", reconstructor._coloredDepthMap);

        // Break the loop if 'a key'q' is pressed
        if (waitKey(1) == 'q') break;
    }
    return 0;
}