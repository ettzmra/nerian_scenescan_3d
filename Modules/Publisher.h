#pragma once
#include "Subscriber.h"

// Forward declaration of Subscriber
//class Subscriber;

class Publisher {
private:
    ImageSet _frame;
    std::vector<Subscriber*> _subscribers;

    // Create an OpenCV VideoCapture object to capture video from the default camera
    cv::VideoCapture _cap;

public:
    DeviceEnumeration::DeviceList _devices;

    Publisher();
    void addSubscriber(Subscriber* subscriber);
    void capture(AsyncTransfer& obj);
    virtual void publish(ImageSet newframe);
    virtual void publish(cv::Mat newframe);
};



