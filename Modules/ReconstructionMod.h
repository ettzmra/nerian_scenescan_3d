#pragma once

//#include "Subscriber.h"
#include "Publisher.h"


class ReconstructionMod : public Subscriber {
private:
    Reconstruct3D _recon3d;
public:
    cv::Mat _coloredDepthMap;
    void captureDepthMap();   
};

