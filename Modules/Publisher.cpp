#include "Publisher.h"

Publisher::Publisher() {
	// Search for Nerian stereo devices
	DeviceEnumeration deviceEnum;
	_devices = deviceEnum.discoverDevices();

	if (_devices.size() == 0) {
		cout << "No Nerian stereo devices discovered! Initializing the default camera..." << endl;

		// Initialize the default camera with OpenCV.
		_cap.open(0);

		if (!_cap.isOpened()) {
			std::cerr << "Error: Could not open the camera." << std::endl;
			exit(EXIT_FAILURE);
		}
	}
	else {
		cout << "Discovered devices:" << endl;
		for (unsigned int i = 0; i < _devices.size(); i++) cout << _devices[i].toString() << endl;
		Sleep(2000);

		_frame.setHeight(480);
		_frame.setWidth(640);
	}
}
void Publisher::addSubscriber(Subscriber* subscriber) {
    _subscribers.push_back(subscriber);
}

void Publisher::capture(AsyncTransfer &obj) {
	if (_cap.isOpened()) {
		cv::Mat frame;

		// Capture a frame via the default camera
		_cap >> frame;

		if (frame.empty()) {
			std::cerr << "Error: Video stream ended." << std::endl;
			exit(EXIT_FAILURE);
		}
		publish(frame);
	}
	else {
		// loop until a frame is captured.
		while (!obj.collectReceivedImageSet(_frame, 0.1 /*timeout*/));
		publish(_frame);
	}
}

void Publisher::publish(ImageSet newframe) {
    for (Subscriber* subscriber : _subscribers) subscriber->onEvent(newframe);
}

void Publisher::publish(cv::Mat newframe) {
    for (Subscriber* subscriber : _subscribers) subscriber->onEvent(newframe);
}