#include "ReconstructionMod.h"

void ReconstructionMod::captureDepthMap() {
    try {
        // Extract the depth map of the frame
        float* zMap = _recon3d.createZMap(_visionTransferFrames_2.front());
        
        // Create an OpenCV Mat to store the depth map
        cv::Mat depthMap(_visionTransferFrames_2.front().getHeight(), _visionTransferFrames_2.front().getWidth(), CV_32FC1, zMap);
        
        cv::Mat resizedDepth;
        cv::resize(depthMap, resizedDepth, cv::Size(640, 480));

        // Normalize the depth map       
        cv::normalize(resizedDepth, resizedDepth, 15, 15000, cv::NORM_MINMAX, CV_8UC3);

        // Apply a color map to the normalized depth map
        cv::applyColorMap(resizedDepth, _coloredDepthMap, cv::COLORMAP_JET);
        _visionTransferFrames_2.pop();
    }
    catch (const std::exception& ex) { std::cerr << "Exception: " << ex.what() << std::endl; }
}
