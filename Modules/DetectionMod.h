#pragma once
#include "Publisher.h"

using namespace cv;

class DetectionMod : public Subscriber {
private:
    Ptr<FaceDetectorYN> _detector;
    Ptr<FaceRecognizerSF> _faceRecognizer;

    map<string, cv::Mat> _detectedPeople;    
    TickMeter _tm;

    int _personCounter = 1;
    float _scale = 1.0;
    double _cosine_similar_thresh = 0.363;
    double _l2norm_similar_thresh = 1.128;

public:   
    cv::Mat _result;
    DetectionMod();  
    void process();
    void detectAndRecognizeFaces(cv::Mat _detectFrame);
    void recognizeBodies() {};
    void detectDimensions() {};
};