#include "DetectionMod.h"

DetectionMod::DetectionMod() {
    try {
        std::string relativePath;
    #ifdef PATH
        relativePath = PATH;
    #endif
        std::string detectionModel = relativePath + "face_detection_yunet_2023mar.onnx";
        std::string recognitionModel = relativePath + "face_recognition_sface_2021dec.onnx";

        // Initialize Face Detector and Face Recognizer:
        _detector = cv::FaceDetectorYN::create(detectionModel, "", Size(640, 480), 0.9, 0.3, 5000);
        _faceRecognizer = cv::FaceRecognizerSF::create(recognitionModel, "");
    } catch (cv::Exception& e) { std::cerr << "OpenCV Exception: " << e.what() << std::endl; }
}

void DetectionMod::process() {
    try {
        if (!_visionTransferFrames_1.empty()) {
            cv::Mat detectionFrame, resizedFrame;
            _visionTransferFrames_1.front().toOpenCVImage(0, detectionFrame);
            cv::resize(detectionFrame, resizedFrame, Size(640, 480), 2.28, 2.28, cv::INTER_LANCZOS4);
            detectAndRecognizeFaces(resizedFrame);
            _visionTransferFrames_1.pop();
        }
        else {
            detectAndRecognizeFaces(_openCvFrames.front());
            _openCvFrames.pop();
        }
    }
    catch (const std::exception& ex) { std::cerr << "Exception occurred: " << ex.what() << std::endl; } 
}

void DetectionMod::detectAndRecognizeFaces(cv:: Mat _detectFrame) {
    // Inference
    cv::Mat faces;
    _tm.start();
    _detector->detect(_detectFrame, faces);
    _tm.stop();

    _result = _detectFrame.clone();

    for (int i = 0; i < faces.rows; i++) {                               
        Mat aligned_face, feature;
        cv::String name;

        // Align and crop facial images of detected faces.
        _faceRecognizer->alignCrop(_detectFrame, faces.row(i), aligned_face);

        // Extract the feature of the given aligned_face.
        _faceRecognizer->feature(aligned_face, feature);
        feature = feature.clone();

        if (!_detectedPeople.empty()) {
            for (auto person : _detectedPeople) {

                // Cosine Similarity : higher value means higher similarity, max 1.0
                double cos_score = _faceRecognizer->match(feature, person.second, FaceRecognizerSF::DisType::FR_COSINE);

                // NormL2 Distance: lower value means higher similarity, min 0.0
                double L2_score = _faceRecognizer->match(feature, person.second, FaceRecognizerSF::DisType::FR_NORM_L2);

                if (cos_score >= _cosine_similar_thresh || L2_score <= _l2norm_similar_thresh) {
                    name = person.first;
                    break;
                }
            }
        }
        if (_detectedPeople.empty() || _detectedPeople.find(name) == _detectedPeople.end()) {
            name = cv::format("Person %d", _personCounter);
            _detectedPeople[name] = feature;
            _personCounter++;
        }

        // Draw a bounding box and put label
        cv::rectangle(_result, Rect2i(int(faces.at<float>(i, 0)), int(faces.at<float>(i, 1)), int(faces.at<float>(i, 2)), int(faces.at<float>(i, 3))), Scalar(0, 255, 0), 2);
        int labelCoordY = int(faces.at<float>(i, 1)) + int(faces.at<float>(i, 3)) + 15;
        cv::putText(_result, name, Point(int(faces.at<float>(i, 0)), labelCoordY), FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0, 255, 0), 2);               
    }
    cv::putText(_result, cv::format("FPS : %.2f", (float)_tm.getFPS()), Point(0, 15), FONT_HERSHEY_SIMPLEX, 0.5, Scalar(0, 255, 0), 2);   
}