#include "Subscriber.h"

void Subscriber::onEvent(ImageSet publishedFrame) {
    _visionTransferFrames_1.push(publishedFrame);
    _visionTransferFrames_2.push(publishedFrame);
}

void Subscriber::onEvent(cv::Mat publishedFrame) {
	_openCvFrames.push(publishedFrame);
}